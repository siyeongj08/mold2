package com.coway.mold.reader;

public class TagData {
    String read_id ;
    long timestamp ; 
    String port ;
    String data ;
    int read ;

    public TagData (){}

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getRead() {
        return read;
    }

    public void setRead(int read) {
        this.read = read;
    }

    public String getRead_id() {
        return read_id;
    }

    public void setRead_id(String read_id) {
        this.read_id = read_id;
    }


    
}
