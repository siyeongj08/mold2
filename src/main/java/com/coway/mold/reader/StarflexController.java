package com.coway.mold.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.coway.mold.common.RfidUtils;
import com.coway.mold.config.CowayConfig;
import com.coway.mold.db1.CommonService;
import com.coway.mold.db1.StarFlexService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class StarflexController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
 
    @Autowired
	CommonService commonService;
	
	@Autowired
	StarFlexService starFlexService ;  

	@Autowired
	CowayConfig cowayConfig ; 

	String TagDataPattern = "^[xA-Z0-9]{20,35}$" ; 

	@RequestMapping(value = "/sf/getConfig", method = { RequestMethod.GET })
	@ResponseBody
	public Object getConfig(@RequestParam final Map<String, Object> params) throws Exception {

		//System.out.println("/sf/getConfig?READER_ID=" + params.get("READER_ID"));

		return new StarNodeConfig(cowayConfig.getSF_MSENDTIME(), cowayConfig.getSF_MCONFIGTIME());
	}  

	
	// @RequestMapping(value = "/sf/upOneData", method = { RequestMethod.GET })
	// @ResponseBody
	// public Object upOneData(@RequestParam final Map<String, Object> params) throws Exception {

	// 	System.out.println("READER_ID=" + params.get("PARAM1") + " timestamp=" + params.get("PARAM4")
	// 		+ " port=" + params.get("PARAM3")+ " data=" + params.get("data") );
		
	// 	String data = params.get("data").toString() ; 
	// 	if(data.length() < 28 || !Pattern.matches(TagDataPattern, data)){  
	// 		return "F" ; 
	// 	}
	// 	data = RfidUtils.hexToAscii(data.substring(6, 28))  ;  
	// 	params.put("PARAM2", data) ; 
  
	// 	String ret = commonService.callProcedureRet(new HashMap<String, Object>() {
	// 		{
	// 			put("procedureName", "PK_RF_GATE.GATE_DATA_ADD");
	// 			put("paramList", new String[] {
	// 				params.get("PARAM1").toString(), 
	// 				params.get("PARAM2").toString(), 
	// 				params.get("PARAM3").toString(), 
	// 				params.get("PARAM4").toString(),  
	// 				"1"
	// 			});
	// 		}
	// 	}) ;  
		
	// 	// if(ret.equals("2") || ret.equals("3") ){ // 입고, 출고
	// 	// 	String ret2 = commonService.callProcedureRet(new HashMap<String, Object>() {
	// 	// 		{
	// 	// 			put("procedureName", "PK_RF_GATE.TAG_GATE_INOUT");
	// 	// 			put("paramList", new String[] {
	// 	// 					params.get("PARAM1").toString(), 
	// 	// 					params.get("PARAM2").toString(),
	// 	// 					ret
	// 	// 			});
	// 	// 		}
	// 	// 	}) ;   
	// 	// }

 
	// 	System.out.println("---ret=" + ret); 

	// 	logger.info(params.get("PARAM1").toString()+","+data +","+ params.get("PARAM3").toString() +","+ params.get("PARAM4").toString() +",1,"+ ret);
		
	// 	return "T" ; 
	// }  




	@RequestMapping(value = "/sf/upJsonData", method = { RequestMethod.POST })
	@ResponseBody
	public String upJsonData(@RequestBody final List<TagData> tagdata) throws Exception {   //@RequestParam final String params,  

		//Map<String,Object> insertMap = new HashMap<String,Object>();//MyBatis에 던질 Map 
		
		List<TagData>  ret = readRfidData(tagdata) ; 

		return "T" ; 
	}


	private List<TagData> readRfidData(List<TagData> alltagData){
		List<TagData> newData = new ArrayList<TagData>(alltagData.size()) ; 
 
		for (TagData tagdata : alltagData) {  
			if(!StringUtils.hasLength(tagdata.getData()) || tagdata.getData().length() < 28 || !Pattern.matches(TagDataPattern, tagdata.getData())) {
				 
			} 
			else{   
				tagdata.setData( RfidUtils.hexToAscii(tagdata.getData().substring(6, 28)) ) ; 
				//tagdata.setTimestamp( tagdata.getTimestamp()/ 1000 ) ; 

				newData.add(tagdata); 
				 
				String ret = commonService.callProcedureRet(new HashMap<String, Object>() {
					{
						put("procedureName", "PK_RF_GATE.GATE_DATA_ADD");
						put("paramList", new String[] {
								tagdata.getRead_id(),
								tagdata.getData(),
								tagdata.getPort(),
								String.valueOf(tagdata.getTimestamp()),
								String.valueOf(tagdata.getRead())	
						});
					}
				}); 
				 
				if(ret.equals("2") || ret.equals("3") ){ // 입고, 출고
					String ret2 = commonService.callProcedureRet(new HashMap<String, Object>() {
						{
							put("procedureName", "PK_RF_GATE.TAG_GATE_INOUT");
							put("paramList", new String[] {
									tagdata.getRead_id(),
									tagdata.getData(),
									ret
							});
						}
					}) ;   
				}
			} 
		} 

		return newData ; 
	} 




}
