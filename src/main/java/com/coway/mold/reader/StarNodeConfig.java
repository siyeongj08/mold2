package com.coway.mold.reader;

public class StarNodeConfig {
 
    int sf_msendtime;
    int sf_mconfigtime;  
    

    public StarNodeConfig() {
    }

    public int getSf_msendtime() {
        return sf_msendtime;
    }

    public void setSf_msendtime(int sf_msendtime) {
        this.sf_msendtime = sf_msendtime;
    }

    public int getSf_mconfigtime() {
        return sf_mconfigtime;
    }

    public void setSf_mconfigtime(int sf_mconfigtime) {
        this.sf_mconfigtime = sf_mconfigtime;
    }

    public StarNodeConfig(int sf_msendtime, int sf_mconfigtime) {
        this.sf_msendtime = sf_msendtime;
        this.sf_mconfigtime = sf_mconfigtime;
    }
 
}
