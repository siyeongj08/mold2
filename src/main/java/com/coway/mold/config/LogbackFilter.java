package com.coway.mold.config;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;

public class LogbackFilter extends Filter<ILoggingEvent> {
	@Override
	public FilterReply decide(ILoggingEvent e) {
		//System.out.println(e.getLoggerName());
		if(e.getMessage().contains("PK_RF_GATE")) {
			return FilterReply.DENY;
		}
		
		return FilterReply.ACCEPT;
		/*
		 * com.coway.mold.reader.StarflexController
		 * com.coway.mold.db1.CommonMapper.returnRet
		 * PK_RF_GATE
		 */	
	}
}
