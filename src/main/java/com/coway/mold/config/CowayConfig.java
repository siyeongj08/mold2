package com.coway.mold.config;

import org.springframework.stereotype.Component;

@Component
public class CowayConfig {

    int SF_MSENDTIME = 5000;// starflex_MAXServerSendTIME = 5000 ;
    int SF_MCONFIGTIME = 1800000;// starflex_MAXTagReadTIME = 1800000 ;

    // CommonService commonService;

    // public CowayConfig(CommonService commonService) {
    // this.commonService = commonService;

    // Map<String, Object> params = new HashMap<>();
    // params = commonService.callProcedureOne(new HashMap<String, Object>() {
    // {
    // put("procedureName", "PK_RF_COMMON.COMMON_SELECT");
    // put("paramList", new ArrayList<String>() {
    // {
    // add("TB_RF_RFIDGATE_MCONFIG");
    // add("");
    // }
    // });
    // }
    // });

    // SF_MSENDTIME = EtcUtils.mapGetInt(params.get("SF_MSENDTIME"));
    // SF_MCONFIGTIME = EtcUtils.mapGetInt(params.get("SF_MCONFIGTIME"));
    // }

    public int getSF_MSENDTIME() {
        return SF_MSENDTIME;
    }

    public void setSF_MSENDTIME(int sF_MSENDTIME) {
        SF_MSENDTIME = sF_MSENDTIME;
    }

    public int getSF_MCONFIGTIME() {
        return SF_MCONFIGTIME;
    }

    public void setSF_MCONFIGTIME(int sF_MCONFIGTIME) {
        SF_MCONFIGTIME = sF_MCONFIGTIME;
    }

}
