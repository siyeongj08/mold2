package com.coway.mold.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.coway.mold.common.EtcUtils;
import com.coway.mold.dto.UserDto;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class LoginInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		HttpSession session = request.getSession();
		UserDto userDto = EtcUtils.getSession(session);
		if (userDto == null) {	//세션 없으면
			log.debug( request.getServletPath() ) ; 
			if (checkAjaxUrl(request.getServletPath())) {
				request.setAttribute("ajaxPreHandle", "-1");
			} else {  
				response.sendRedirect("/login"); 
			}
			return false;
		}

		return true;
	}

	private boolean checkAjaxUrl(String url) {
		int index = url.lastIndexOf("/");
		if (index == -1) {
			return false;
		}
		// /now 페이지 때문에 exception 발생
		if (url.substring(index).startsWith("/ajax")) {
			return true;
		}
		return false;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object o, ModelAndView model)
			throws Exception {

		if (model != null) {
			HttpSession session = request.getSession();
			//UserDto userDto = EtcUtils.getSession(session);

			// if (userDto != null) {
			// 	model.addObject("userDto", userDto);
			// 	// model.addObject("menuElement", userDto.getMenuElement());
			// }
		}

	}

}
