package com.coway.mold.db1;

import java.util.HashMap;
import java.util.Map;

import com.coway.mold.common.ResultVo;
import com.coway.mold.dto.ParamObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/db1")
public class CommonController {

    @Autowired
    CommonService commonService;
    
    // return 형식 다량의 row
    @RequestMapping(value = "/callProcedureList", method = { RequestMethod.POST })
    @ResponseBody
    public ResultVo callProcedureList(
        @ModelAttribute ParamObject paramObject) throws Exception {
            return createAjaxResultVo(commonService.callProcedureList(setParamMap(paramObject.getProcedureName(), paramObject.getParamList())));
            //return createAjaxResultVo(commonService.callProcedureList(paramObject));
    }

    
    // return 형식 1개의 row
    @RequestMapping(value = "/callProcedureOne", method = { RequestMethod.POST })
    @ResponseBody
    public ResultVo callProcedureOne(
        @ModelAttribute ParamObject paramObject) throws Exception {
            return createAjaxResultVo(commonService.callProcedureOne(setParamMap(paramObject.getProcedureName(), paramObject.getParamList())));
    }

    // return 형식 1개의 varchar2 ret
    @RequestMapping(value = "/callProcedureRet", method = { RequestMethod.POST })
    @ResponseBody
    public ResultVo callProcedureRet(
        @ModelAttribute ParamObject paramObject) throws Exception {
            return createAjaxResultVo(commonService.callProcedureRet(setParamMap(paramObject.getProcedureName(), paramObject.getParamList())));
    }


    // return 형식 다량의 row
    @RequestMapping(value = "/callProcedureList2", method = { RequestMethod.POST })
    @ResponseBody
    public ResultVo callProcedureList2( @RequestBody ParamObject paramObject  ) throws Exception {
            return createAjaxResultVo(commonService.callProcedureList(setParamMap(paramObject.getProcedureName(), paramObject.getParamList())));
            //return createAjaxResultVo(commonService.callProcedureList(paramObject));
    }

    
    // return 형식 1개의 row
    @RequestMapping(value = "/callProcedureOne2", method = { RequestMethod.POST })
    @ResponseBody
    public ResultVo callProcedureOne2(@RequestBody ParamObject paramObject   ) throws Exception {
            return createAjaxResultVo(commonService.callProcedureOne(setParamMap(paramObject.getProcedureName(), paramObject.getParamList())));
    }

    // return 형식 1개의 varchar2 ret
    @RequestMapping(value = "/callProcedureRet2", method = { RequestMethod.POST })
    @ResponseBody
    public ResultVo callProcedureRet2(@RequestBody ParamObject paramObject  ) throws Exception {
            return createAjaxResultVo(commonService.callProcedureRet(setParamMap(paramObject.getProcedureName(), paramObject.getParamList())));
    }


    



    private ResultVo createAjaxResultVo(Object procedureResult){
        ResultVo resultVo = new ResultVo();
        resultVo.setData(procedureResult);
        resultVo.setRetcd(1); // 성공을 의미하는 1 / 클라이언트에서 이것으로 ajax가 정상적으로 호출되었는지 판단한다.
        return resultVo;
    }

    
	public static Map<String, Object> setParamMap(String procedureName, String[] paramList){
        if (paramList == null){
            paramList = new String[0];
        }
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("procedureName", procedureName);

        for (int i = 0; i < paramList.length; i ++){ // 대소문자 구분 안하게
            paramList[i] = paramList[i].toUpperCase();
        }

        params.put("paramList", paramList);
        return params;
    }


}