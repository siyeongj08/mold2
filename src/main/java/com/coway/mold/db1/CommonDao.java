package com.coway.mold.db1;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository("CommonDao")
public class CommonDao {

	@Autowired
	@Qualifier("sqlSessionTemplate")
	private SqlSessionTemplate sqlSession;

	public List<Map<String, Object>> returnCur(Map<String, Object> params) {
		return sqlSession.selectOne("returnCur", params);
	}

	public List<Map<String, Object>> returnRet(Map<String, Object> params) {
		return sqlSession.selectOne("returnRet", params);
	}

}
