package com.coway.mold.db1;

import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("MassService")
@Transactional 
public class  MassService {

	@Autowired
	@Qualifier("sqlSessionTemplate")
	private SqlSessionTemplate sqlSession;
 
	
	public int updateAuthAllowSite(Map<String, Object> params){

		sqlSession.delete("deleteStarFlexLog", params) ; 

		return sqlSession.insert("insertAuthAllowSite", params);
	}
 
}
