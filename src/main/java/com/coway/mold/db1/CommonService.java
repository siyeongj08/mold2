package com.coway.mold.db1;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service("CommonService")
@Transactional
public class CommonService {

	@Autowired
	private CommonDao mapper;

	public List<Map<String, Object>> callProcedureList(Map<String, Object> params) {
		mapper.returnCur(params);
		return listAllRow(params);
	}

	public Map<String, Object> callProcedureOne(Map<String, Object> params) {
		mapper.returnCur(params);
		return listFirstRow(params);
	}

	public String callProcedureRet(Map<String, Object> params) {
		mapper.returnRet(params);
		return getObject(params);
	}

	private List<Map<String, Object>> listAllRow(Map<String, Object> params) {
		ArrayList<Map<String, Object>> list = (ArrayList<Map<String, Object>>) params.get("CUR");
		if (list != null && list.size() > 0) {
			return list;
		}
		return null;
	}
	
	private Map<String, Object> listFirstRow(Map<String, Object> params) {
		ArrayList<Map<String, Object>> list = (ArrayList<Map<String, Object>>) params.get("CUR");
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	private String getObject(Map<String, Object> params) {
		String val = (String) params.get("RET");
		if (val == null || val == "") {
			return null;
		}

		return val;
	}

}