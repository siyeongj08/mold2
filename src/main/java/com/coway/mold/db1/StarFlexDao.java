package com.coway.mold.db1;

import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;


@Repository("StarFlexDao")
public class  StarFlexDao {

	@Autowired
	@Qualifier("sqlSessionTemplate")
	private SqlSessionTemplate sqlSession;
 
	
	public int insertStarFlexLog(Map<String, Object> params){
		return sqlSession.insert("insertStarFlexLog", params);
	}
 
}
