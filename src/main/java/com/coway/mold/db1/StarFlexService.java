package com.coway.mold.db1 ; 

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("StarFlexService")
@Transactional
public class StarFlexService {

	@Autowired
	private StarFlexDao mapper; 
		
	public int insertStarFlexLog(Map<String, Object> params) {
		return mapper.insertStarFlexLog(params);
	}

}