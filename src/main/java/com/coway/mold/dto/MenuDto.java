package com.coway.mold.dto;

import java.io.Serializable;

public class MenuDto implements Serializable {
    private static final long serialVersionUID = 4873035241786182838L;

    String id;
    String text;
    String url ;
 
    public MenuDto() {
    }

    public MenuDto(String id, String text, String url) {
        this.id = id;
        this.text = text;
        this.url = url;
    }
    
    
    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof MenuDto)) {
            return false;
        }
        MenuDto menuDto = (MenuDto) o;
        return id == menuDto.id  ;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
}