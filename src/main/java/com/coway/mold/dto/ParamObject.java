package com.coway.mold.dto;

public class ParamObject {
     
    String procedureName ; 
    String[] paramList ;

    
    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public String[] getParamList() {
        return paramList;
    }

    public void setParamList(String[] paramList) {
        this.paramList = paramList;
    }

    public ParamObject() {
    }

    public ParamObject(String procedureName, String[] paramList) {
        this.procedureName = procedureName;
        this.paramList = paramList;
    }

    
}
