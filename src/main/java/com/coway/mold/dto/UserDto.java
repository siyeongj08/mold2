package com.coway.mold.dto;

import java.io.Serializable;

//import com.exax.visionmd.common.Menu.MenuTreeVo;

public class UserDto implements Serializable {

    private static final long serialVersionUID = 4873035241786182838L;

    int seq;
    String name;
    String rank;
    String birth;
    String call;
    String mail;
    String id;
    String pw;
    String auth;
    String del_yn;
    String auth_group_code;
    String menuElement;
    String vendor_code;
    String vendor_name;
    String act_seq;
    String act_yr;
    String act_dgre;
    String act_content;
    String ofse_apdx_file_no;
    String sign_apdx_file_no;
    // MenuTreeVo menuTreeVo;  

    // List<ColumnDto> columnList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getCall() {
        return call;
    }

    public void setCall(String call) {
        this.call = call;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public String getDel_yn() {
        return del_yn;
    }

    public void setDel_yn(String del_yn) {
        this.del_yn = del_yn;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getAuth_group_code() {
        return auth_group_code;
    }

    public void setAuth_group_code(String auth_group_code) {
        this.auth_group_code = auth_group_code;
    }

    public String getMenuElement() {
        return menuElement;
    }

    public void setMenuElement(String menuElement) {
        this.menuElement = menuElement;
    }

    public String getVendor_code() {
        return vendor_code;
    }

    public void setVendor_code(String vendor_code) {
        this.vendor_code = vendor_code;
    }

    public String getVendor_name() {
        return vendor_name;
    }

    public void setVendor_name(String vendor_name) {
        this.vendor_name = vendor_name;
    }

    public String getAct_seq() {
        return act_seq;
    }

    public void setAct_seq(String act_seq) {
        this.act_seq = act_seq;
    }

    public String getAct_yr() {
        return act_yr;
    }

    public void setAct_yr(String act_yr) {
        this.act_yr = act_yr;
    }

    public String getAct_dgre() {
        return act_dgre;
    }

    public void setAct_dgre(String act_dgre) {
        this.act_dgre = act_dgre;
    }

    public String getAct_content() {
        return act_content;
    }

    public void setAct_content(String act_content) {
        this.act_content = act_content;
    }

    public String getOfse_apdx_file_no() {
        return ofse_apdx_file_no;
    }

    public void setOfse_apdx_file_no(String ofse_apdx_file_no) {
        this.ofse_apdx_file_no = ofse_apdx_file_no;
    }

    public String getSign_apdx_file_no() {
        return sign_apdx_file_no;
    }

    public void setSign_apdx_file_no(String sign_apdx_file_no) {
        this.sign_apdx_file_no = sign_apdx_file_no;
    }
}