package com.coway.mold.dto;

import java.io.Serializable;

public class ColumnDto implements Serializable {
    private static final long serialVersionUID = 4873035241786182838L;

    String seq;
    String id;
    String url;
    String tableId;
    String columnlist;
    String columnname;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getColumnlist() {
        return columnlist;
    }

    public void setColumnlist(String columnlist) {
        this.columnlist = columnlist;
    }

    public String getColumnname() {
        return columnname;
    }

    public void setColumnname(String columnname) {
        this.columnname = columnname;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }
}