package com.coway.mold.thytag;

import java.util.List;
import java.util.Map;

public class SiteTag {

    public String getMenuTree(List<Map<String, Object>> list, String nowUrl) {
        String pregroup = "";
        String predepth = "";
        String group = "";
        String depth = "";
        StringBuffer buffer = new StringBuffer();

        for (Map<String, Object> map : list) {
            group = map.get("MGROUP").toString();
            depth = map.get("DEPTH").toString();
            String morder = map.get("MORDER").toString();

            if (!pregroup.equals(group)) {
                if (!predepth.equals("")) {
                    if (!predepth.equals(depth)) {
                        buffer.append("</ul>\n");
                    }
                    buffer.append("</li>\n");
                }
                buffer.append("<li>\n");

                if (morder.equals("0")) {
                    buffer.append("<a href='javascript: void(0);'  ><i class='feather-list'></i>"
                            + map.get("TITLE").toString() + "<span class='fa fa-chevron-down'></span></a>\n");
                } else {
                    buffer.append("<li><a href='javascript: void(0);' ><span>" + map.get("TITLE").toString()
                            + "</span></a></li>\n");
                }
            } else {
                if (!predepth.equals(depth)) {
                    buffer.append("<ul class='nav child_menu' >\n");
                }

                if (morder.equals("0")) {
                    buffer.append("<a href='javascript: void(0);'  ><i class='feather-list'></i>"
                            + map.get("TITLE").toString() + "<span class='fa fa-chevron-down'></span></a>\n");
                } else {
                    buffer.append("<li><a href='javascript: void(0);' ><span>" + map.get("TITLE").toString()
                            + "</span></a></li>\n");
                }
            }

            pregroup = group;
            predepth = depth;
        }

        if (!pregroup.equals(group) && !predepth.equals(depth)) {
            buffer.append("</ul>\n");
        }
        buffer.append("</li>\n");

        return buffer.toString();
    }
}