package com.coway.mold.thytag;

import java.util.Set;
import java.util.Collections;
 
import org.springframework.stereotype.Component;
import org.thymeleaf.context.IExpressionContext;
import org.thymeleaf.dialect.AbstractDialect;
import org.thymeleaf.dialect.IExpressionObjectDialect;
import org.thymeleaf.expression.IExpressionObjectFactory;


@Component  
public class SiteTagDialect extends AbstractDialect implements IExpressionObjectDialect {

    protected SiteTagDialect() {
        super("siteTag");
    }
 
    @Override
    public IExpressionObjectFactory getExpressionObjectFactory() {
        return new IExpressionObjectFactory() {

            @Override
            public Set<String> getAllExpressionObjectNames() {
                return Collections.singleton("siteTag");
            }

            @Override
            public Object buildObject(IExpressionContext context, String expressionObjectName) {
                return new SiteTag();
            }

            @Override
            public boolean isCacheable(String expressionObjectName) {
                return true;
            }
        };
    } 
}