package com.coway.mold;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

//import com.m8trx.util.system.SystemServiceInstaller;

import com.coway.mold.common.SFTPUtils;

@SpringBootApplication
public class MoldApplication{

	public static void main(String[] args) {
		// SystemServiceInstaller.installIfRequested(args);
		SpringApplication.run(MoldApplication.class, args);

		//testSft();
	}

	/*
	 * private static void testSft() {
	 * 
	 * SFTPUtils sftp = new SFTPUtils();
	 * 
	 * try { System.out.println("====testSft===1==") ; sftp.init("10.101.1.194",
	 * "weblogic", "weblogic", 22); System.out.println("====testSft==2===") ;
	 * 
	 * // java.io.InputStream in = sftp.download("/home/cwuser/", "Test.java"); //
	 * java.nio.file.Files.copy(in, new java.io.File("c:/Work/.profile").toPath(),
	 * // java.nio.file.StandardCopyOption.REPLACE_EXISTING);
	 * 
	 * java.io.File file = new java.io.File("/home/cwuser/d1.png") ;
	 * sftp.upload("/woongjin/file-repository/wtms/rfid_mold_image/" , file ) ;
	 * 
	 * 
	 * } catch (Exception e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); }
	 * 
	 * System.out.println("====testSft===9==") ; }
	 */

 
	//소수의 정의: 1과 자기 자신만으로 나누어 떨어지는 1보다 큰 양의 정수
	// public void GetPrimeNum(int inputNum){ // 입력 파라매터 : 원하는 숫자
	// 	List<Integer> filterNumList = new ArrayList<Integer>(); // List 선언
	// 	for (int i=2; i < inputNum; i++){ // i 부터 입력 파라매터까지 루프
	// 		if (filterNumList.contains(i)){ // i가 있으면
	// 			continue; // 소수가 아닌 것으로 판단
	// 		}
	// 		else{
	// 			System.out.println(i + ""); // 숫자 + 공백문자 로 숫자를 문자로 만드는 꼼수
	// 			int filterNum = i; // 앞으로 필터링할 숫자
	// 			while(filterNum < inputNum){ // 필터링할 숫자가 입력 숫자보다 작으면 반복
	// 				filterNum *= 2;
	// 				filterNumList.add(filterNum); // 필터링할 숫자에 추가
	// 			}
	// 		}
	// 	}
	// }

}
