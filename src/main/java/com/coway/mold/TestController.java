package com.coway.mold;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam; 

@Controller
public class TestController {

	
	@RequestMapping(value = { "/test/{path}" })
	public String test(@PathVariable("path") String path, HttpServletRequest
		request, HttpServletResponse response,
		HttpSession session, @RequestParam final Map<String, Object> params, final
		Model model) throws Exception {

		return "pages/" + path;
	}


	// @Autowired
	// CommonService commonService;

	// @Autowired
	// PathUtils pathUtils;

	// @Autowired private ResourceLoader resourceLoader;
	// // @Autowired private ServletContext servletContext;

	// @RequestMapping(value = { "/scan" })
	// public String scan(HttpServletRequest request, HttpServletResponse response,
	// HttpSession session,
	// @RequestParam final Map<String, Object> params, final Model model) throws
	// Exception {

	// System.out.println(System.getProperty("os.name"));

	// System.out.println(pathUtils.getUpLoadImgUrl() );
	// // System.out.println(resourceLoader.getResource("/").getURI().getPath());
	// //
	// System.out.println(resourceLoader.getResource("resources/application.properties").getURI().getPath());

	// return "pages/web/login";
	// }

	// @RequestMapping(value = { "/main" })
	// public String main(HttpServletRequest request, HttpServletResponse response,
	// HttpSession session,
	// @RequestParam final Map<String, Object> params, final Model model) throws
	// Exception {

	// params.put("SP_NAME", "SP_RF_LIST_COMMON");
	// params.put("PARAM1", "SITE_MENU");
	// params.put("PARAM2", "");
	// params.put("PARAM3", " order by mgroup , depth , morder ");

	// model.addAttribute("sitemenu", commonService.spListParam3(params));

	// return "pages/web/main/main";
	// }

	// @RequestMapping(value = { "/test/{path}" })
	// public String test(@PathVariable("path") String path, HttpServletRequest
	// request, HttpServletResponse response,
	// HttpSession session, @RequestParam final Map<String, Object> params, final
	// Model model) throws Exception {

	// return "pages/web/" + path;
	// }

	// @RequestMapping(value = { "/mold/{path}" })
	// public String path(@PathVariable("path") String path, HttpServletRequest
	// request, HttpServletResponse response,
	// HttpSession session, @RequestParam final Map<String, Object> params, final
	// Model model) throws Exception {

	// params.put("SP_NAME", "SP_RF_LIST_COMMON");
	// params.put("PARAM1", "SITE_MENU");
	// params.put("PARAM2", "");
	// params.put("PARAM3", " order by mgroup , depth , morder ");

	// model.addAttribute("sitemenu", commonService.spListParam3(params));

	// return "pages/web/mold/" + path;
	// }

	// @RequestMapping(value = { "/user/{path}" })
	// public String user(@PathVariable("path") String path, HttpServletRequest
	// request, HttpServletResponse response,
	// HttpSession session, @RequestParam final Map<String, Object> params, final
	// Model model) throws Exception {

	// params.put("SP_NAME", "SP_RF_LIST_COMMON");
	// params.put("PARAM1", "SITE_MENU");
	// params.put("PARAM2", "");
	// params.put("PARAM3", " order by mgroup , depth , morder ");

	// model.addAttribute("sitemenu", commonService.spListParam3(params));

	// return "pages/web/user/" + path;
	// }

	// @RequestMapping(value = "/testjson", method = { RequestMethod.GET })
	// @ResponseBody
	// public Object testjson(@RequestParam final Map<String, Object> params) throws
	// Exception {

	// return new Config();
	// }

	// @RequestMapping(value = "/upJsonData", method = { RequestMethod.POST })
	// @ResponseBody
	// public void upJsonData(@RequestBody final List<TagData> params) throws
	// Exception {

	// for (TagData tagData : params) {
	// System.out.println(tagData.timestamp);
	// }

	// System.out.println("----end);");

	// // return new ResultVo();
	// }

	// @RequestMapping(value = "/biznumcheck", method = { RequestMethod.GET })
	// public String biznumcheck(final Model model) throws Exception {

	// WebDriver driver = null;
	// try {
	// System.setProperty("webdriver.chrome.driver",
	// "C:/Work/VSCode/Mojixs/Coway/mold/doc/lib/chromedriver_win32/chromedriver.exe");
	// ChromeOptions options = new ChromeOptions();
	// options.setHeadless(true);
	// driver = new ChromeDriver(options);

	// driver.get("https://teht.hometax.go.kr/websquare/websquare.html?w2xPath=/ui/ab/a/a/UTEABAAA13.xml");

	// Thread.sleep(1000);

	// driver.findElement(By.id("bsno")).sendKeys("1148193374");

	// driver.findElement(By.id("trigger5")).click();

	// String ret = driver.findElement(By.id("grid2_body_table")).getText();
	// System.out.println(ret);

	// model.addAttribute("ret", ret);
	// } catch (Exception e) {

	// } finally {
	// if (driver != null)
	// driver.close();
	// }

	// return "pages/web/login";
	// }

	// public class Config {
	// public String MAXEventTIME = "20000";
	// public String MAXServerSendTIME = "5000";
	// public String MAXTagReadTIME = "1800000";
	// }

}
