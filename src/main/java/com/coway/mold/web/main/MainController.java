package com.coway.mold.web.main ; 

import java.util.Map;

import com.coway.mold.common.BaseController;
import com.coway.mold.dto.UserDto;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

@Controller
@RequestMapping("/web/main")
public class MainController extends BaseController {

    
	@RequestMapping(value = { "/dashboard" }) 
	public String main(@RequestParam final Map<String, Object> params, final Model model,
		@SessionAttribute("UserDto") UserDto userDto) throws Exception {

		return "pages/web/main/dashboard";
	}
}
