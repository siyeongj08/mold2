package com.coway.mold.web.system;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.io.FileReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.coway.mold.common.BaseController;
import com.coway.mold.common.EtcUtils;
import com.coway.mold.common.ResultVo;
import com.coway.mold.db1.MassService;
import com.coway.mold.dto.UserDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;

@Controller
@RequestMapping("/web/system")
public class SystemController extends BaseController {

    @Autowired
    MassService massService ; 


    @RequestMapping(value = { "/gateConfig" })
    public String gateConfig(HttpServletRequest request, HttpServletResponse response, final Model model,
            @SessionAttribute("UserDto") UserDto userDto) throws Exception {

        return "pages/web/system/gateConfig";
    }

    @RequestMapping(value = { "/gateScanLog" })
    public String gateScanLog(HttpServletRequest request, HttpServletResponse response, final Model model,
            @SessionAttribute("UserDto") UserDto userDto) throws Exception {

        return "pages/web/system/gateScanLog";
    }

    @RequestMapping(value = { "/menuConfig" })
    public String menuConfig(HttpServletRequest request, HttpServletResponse response, final Model model,
            @SessionAttribute("UserDto") UserDto userDto) throws Exception {

        return "pages/web/system/menuConfig";
    }



	// 세션이 살아있는지 체크
	@RequestMapping(value = "/ajaxUpdateAuthAllowSite", method = { RequestMethod.POST })
	@ResponseBody
	public ResultVo updateAuthAllowSite(HttpServletRequest request, @RequestParam final Map<String, Object> params ) throws Exception {
		ResultVo resultVo = EtcUtils.checkAjaxSession(request);
		if (resultVo != null) { 

            if (!EtcUtils.hasLength(params.get("code")) ) {
                resultVo.setMessage("code값 필요");
                return resultVo;
            }  
             
            List<String> valueslist = Arrays.asList(params.get("values").toString().split(",", -1)); 

            params.put("list", valueslist) ; 
 
            massService.updateAuthAllowSite(params) ; 
            resultVo.setRetcd(1);

			return resultVo; // 세션데이터가 있을때
        }
        
		return new ResultVo(-1); // 세션데이터가 없으면
 
    }

	
	@RequestMapping(value = "/ajaxGetFileLog", method = { RequestMethod.GET })
	@ResponseBody
	public Object ajaxGetFileLog(@RequestParam final Map<String, Object> params) throws Exception { 

		StringBuffer buffer = new StringBuffer("") ; 

		List<String> list = Files.readAllLines(Paths.get("/usr/local/apache-tomcat-9.0.41/logs/logs/rfid/logdate.log"), StandardCharsets.UTF_8);
		Collections.reverse(list);
		int i=0 ; 
		for (String line : list) {
			//System.out.println(line);
			buffer.append(line + "\n") ; 
			if(i>100)
				break;
			
			i++;
		} 

		return buffer.toString() ;
	}  


}
