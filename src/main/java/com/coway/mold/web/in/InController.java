package com.coway.mold.web.in;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.coway.mold.common.BaseController;
import com.coway.mold.dto.UserDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequestMapping("/web/in")
public class InController extends BaseController {

	@RequestMapping(value = { "/inReg" })
	public String inReg(HttpServletRequest request, HttpServletResponse response, final Model model,
			@SessionAttribute("UserDto") UserDto userDto) throws Exception {

		return "pages/web/in/inReg";
	}

	@RequestMapping(value = { "/newReg" })
	public String newReg(HttpServletRequest request, HttpServletResponse response, final Model model,
			@SessionAttribute("UserDto") UserDto userDto) throws Exception {

		return "pages/web/in/newReg";
	}

		
	@RequestMapping(value = { "/notApproveIn" })
	public String notApproveIn(HttpServletRequest request, HttpServletResponse response, final Model model,
			@SessionAttribute("UserDto") UserDto userDto) throws Exception {

		return "pages/web/in/notApproveIn";
	}
	
	@RequestMapping(value = { "/systemOpenReg" })
	public String systemOpenReg(HttpServletRequest request, HttpServletResponse response, final Model model,
			@SessionAttribute("UserDto") UserDto userDto) throws Exception {

		return "pages/web/in/systemOpenReg";
	}

	
}
