package com.coway.mold.web.appUpdate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.coway.mold.common.BaseController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/web/appUpdate")
public class AppUpdateController extends BaseController {

    @RequestMapping(value = { "/appUpdate" })
    public String appUpdate(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {

        return "pages/web/appUpdate/appUpdate";
    }
}