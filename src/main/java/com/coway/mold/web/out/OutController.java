package com.coway.mold.web.out;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.coway.mold.common.BaseController;
import com.coway.mold.db1.CommonService;
import com.coway.mold.dto.UserDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

@Controller
@RequestMapping("/web/out")
public class OutController extends BaseController {

	@Autowired
	CommonService commonService;

	@RequestMapping(value = { "/request" })
	public String request(HttpServletRequest request, HttpServletResponse response, final Model model,
			@SessionAttribute("UserDto") UserDto userDto) throws Exception {

		model.addAttribute("rantEntp", commonService.callProcedureList(new HashMap<String, Object>() {
			{
				put("procedureName", "PK_RF_ENTP.ENTP_SELECT");
				put("paramList", new String[] {
						"1" // 대여업체
				});
			}
		}));

		model.addAttribute("saveEntp", commonService.callProcedureList(new HashMap<String, Object>() {
			{
				put("procedureName", "PK_RF_ENTP.ENTP_SELECT");
				put("paramList", new String[] {
						"2" // 보관업체
				});
			}
		}));

		model.addAttribute("toolsEntp", commonService.callProcedureList(new HashMap<String, Object>() {
			{
				put("procedureName", "PK_RF_ENTP.ENTP_SELECT");
				put("paramList", new String[] {
						"3" // 금형업체
				});
			}
		}));

		return "pages/web/out/request";  
	}

	@RequestMapping(value = { "/approve" })
	public String approve(HttpServletRequest request, HttpServletResponse response, final Model model,
			@SessionAttribute("UserDto") UserDto userDto) throws Exception {

		return "pages/web/out/approve";
	}

	@RequestMapping(value = { "/outReg" })
	public String outReg(HttpServletRequest request, HttpServletResponse response, final Model model,
			@SessionAttribute("UserDto") UserDto userDto) throws Exception {

		return "pages/web/out/outReg";
	}	 

	@RequestMapping(value = { "/notYetIn" })
	public String notYetIn(HttpServletRequest request, HttpServletResponse response, final Model model,
			@SessionAttribute("UserDto") UserDto userDto) throws Exception {

		return "pages/web/out/notYetIn";
	}

	
}
