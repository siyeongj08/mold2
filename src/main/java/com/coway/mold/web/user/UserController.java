package com.coway.mold.web.user;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.resource.PathResourceResolver;

import com.coway.mold.common.BaseController;
import com.coway.mold.common.PathUtils;
import com.coway.mold.common.ResultFileVo;
import com.coway.mold.common.SFTPUtils;
import com.coway.mold.common.UploadFileUtils;
import com.coway.mold.dto.UserDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/web/user")
public class UserController extends BaseController {
    
    @Value("${WTMSFTP_IP}")
	String WTMSFTP_IP ; 
    @Value("${WTMSFTP_PORT}")
	int WTMSFTP_PORT ;  
    @Value("${WTMSFTP_ID}")
	String WTMSFTP_ID  ;  
	@Value("${WTMSFTP_PW}")
	String WTMSFTP_PW  ;  
	@Value("${WTMSFTP_PATH}")
	String WTMSFTP_PATH  ; 
	@Value("${WTMSFTP_FOLDER}")
	String WTMSFTP_FOLDER  ; 
	@Value("${WTMSFTP_IMG_URL}")
	String WTMSFTP_IMG_URL  ;
	@Value("${server.upload.dir}")
	String SERVER_UPLOAD_DIR;
	
	/*
	 * @Autowired PathUtils pathUtils;
	 */

    @ResponseBody
    @RequestMapping(value = "/ajaxUploadPic", method = RequestMethod.POST, produces = "application/json")
    public ResultFileVo ajaxUploadPic(MultipartHttpServletRequest mulitifilelist, @SessionAttribute("UserDto") UserDto userDto) 
    																					throws Exception {
    	
        // 파일 저장
        ResultFileVo resultFileVo = UploadFileUtils.fileSaveCrop(20, 50, SERVER_UPLOAD_DIR + "/", mulitifilelist);
        
        resultFileVo.setFileUrl(SERVER_UPLOAD_DIR + resultFileVo.getFilePath());
        
		
		if( !sftpSend(resultFileVo.getFileUrl()) ) {
			log.debug("ajaxUploadPic !sftpSend") ; 
			return resultFileVo; 
		}
		
		fileServerSendDBInsert( "1" , resultFileVo , userDto.getVendor_code()) ;
		
		resultFileVo.setSuccess(true); resultFileVo.setFileName( WTMSFTP_IMG_URL +
		resultFileVo.getFileName());
		

        return resultFileVo;
    }

    @ResponseBody
    @RequestMapping(value = "/ajaxSaveBase64", method = RequestMethod.POST, produces = "application/json")
    public ResultFileVo ajaxSaveBase64(@RequestParam final Map<String, Object> params,
            @SessionAttribute("UserDto") UserDto userDto) throws Exception {

        //String type = params.get("type").toString();
        String imgBase64 = params.get("imgBase64").toString();
        imgBase64 = imgBase64.substring(imgBase64.indexOf(",") + 1);

        /* sourceforge에서 배포하는 Base64 클래스를 사용하면 가장 간단하게 디코딩과 이미지 파일에 저장을 동시에 처리한다 */
        // Base64.decodeToFile(imgBase64, "d:/test/decodedImg.png"); //jpg,png ok

        // java.util.Base64 클래스를 사용하여 디코딩한 후에 ImageIO를 이용하여 이미지 파일에 저장한다
        byte[] decodedBytes = Base64.getDecoder().decode(imgBase64); // java.util.Base64
        
        ResultFileVo resultFileVo = UploadFileUtils.filesave(SERVER_UPLOAD_DIR + "/", "ceo.png", decodedBytes);

        // 파일 저장
        resultFileVo.setFileUrl(SERVER_UPLOAD_DIR + resultFileVo.getFilePath());
        
        if( !sftpSend(resultFileVo.getFileUrl()) ) { 
            return resultFileVo; 
        }
        
        fileServerSendDBInsert( "2" , resultFileVo , userDto.getVendor_code()) ; 
          
        resultFileVo.setSuccess(true);
        resultFileVo.setFileName( WTMSFTP_IMG_URL + resultFileVo.getFileName());

        return resultFileVo;
    } 
    
	private boolean sftpSend(String localfile) throws Exception {
        SFTPUtils sftp = new SFTPUtils();
        sftp.init(WTMSFTP_IP , WTMSFTP_ID , WTMSFTP_PW, WTMSFTP_PORT );

        java.io.File file = new java.io.File(localfile);

        return sftp.upload(WTMSFTP_PATH + WTMSFTP_FOLDER , file);
	}
    
    
    @RequestMapping(value = { "/stampup" })
    public String stampup(@RequestParam final Map<String, Object> params, final Model model,
            @SessionAttribute("UserDto") UserDto userDto) throws Exception {
        return "pages/web/user/stampup";
    }

    @RequestMapping(value = { "/profile" })
    public String profile(@RequestParam final Map<String, Object> params, final Model model,
            @SessionAttribute("UserDto") UserDto userDto) throws Exception {
        return "pages/web/user/profile";
    }

    private void fileServerSendDBInsert(String type , ResultFileVo resultFileVo , String vender_code) throws Exception {
    	log.debug("fileServerSendDBInsert : " + WTMSFTP_PATH + WTMSFTP_FOLDER + "/" + resultFileVo.getFileName());
        //if(sendFileServer( resultFileVo.getFileUrl())){ 
            String ret = commonService.callProcedureRet(new HashMap<String, Object>() {
                {
                    put("procedureName", "PK_RF_USER.USER_FILE_INSERT");
                    put("paramList", new String[] { 
                            type,
                            resultFileVo.getOriginalFilename(),
                            WTMSFTP_PATH + WTMSFTP_FOLDER + "/" + resultFileVo.getFileName(),
                            String.valueOf(resultFileVo.getFilesize()),
                            resultFileVo.getFileExt(),
                            resultFileVo.getFileName(),
                            WTMSFTP_FOLDER,
                            vender_code
                    });
                }
            });  
        //}
    }

}
