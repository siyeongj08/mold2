package com.coway.mold.web.stock;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.coway.mold.common.BaseController;
import com.coway.mold.dto.UserDto;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

@Controller
@RequestMapping("/web/stock")
public class StockController extends BaseController {

	@RequestMapping(value = { "/show" })
	public String show(HttpServletRequest request, HttpServletResponse response, final Model model,
			@SessionAttribute("UserDto") UserDto userDto) throws Exception {
				model.addAttribute("rantEntp", commonService.callProcedureList(new HashMap<String, Object>() {
					{
						put("procedureName", "PK_RF_ENTP.ENTP_SELECT");
						put("paramList", new String[] {
								"1" // 대여업체
						});
					}
				}));

		return "pages/web/stock/show";
	}

	@RequestMapping(value = { "/actReg" })
	public String actReg(HttpServletRequest request, HttpServletResponse response, final Model model,
			@SessionAttribute("UserDto") UserDto userDto) throws Exception {

		return "pages/web/stock/actReg";
	}

	@RequestMapping(value = { "/history" })
	public String history(HttpServletRequest request, HttpServletResponse response, final Model model,
			@SessionAttribute("UserDto") UserDto userDto) throws Exception {

		return "pages/web/stock/history";
	}

	@RequestMapping(value = { "/total" })
	public String total(HttpServletRequest request, HttpServletResponse response, final Model model,
			@SessionAttribute("UserDto") UserDto userDto) throws Exception {

		return "pages/web/stock/total";
	}

	@RequestMapping(value = { "/disposal" })
	public String disposal(HttpServletRequest request, HttpServletResponse response, final Model model,
			@SessionAttribute("UserDto") UserDto userDto) throws Exception {

		return "pages/web/stock/disposal";
	}

	@RequestMapping(value = { "/actLess" })
	public String actLess(HttpServletRequest request, HttpServletResponse response, final Model model,
			@SessionAttribute("UserDto") UserDto userDto) throws Exception {

		return "pages/web/stock/actLess";
	}

	@RequestMapping(value = { "/actList" })
	public String actList(HttpServletRequest request, HttpServletResponse response, final Model model,
			@SessionAttribute("UserDto") UserDto userDto) throws Exception {
		return "pages/web/stock/actList";
	}

	@RequestMapping(value = { "/actListSub" })
	public String actListSub(HttpServletRequest request, HttpServletResponse response, final Model model,
			@SessionAttribute("UserDto") UserDto userDto) throws Exception {


			model.addAttribute("mprdEntpList", commonService.callProcedureList(new HashMap<String, Object>() {
				{
					put("procedureName", "PK_RF_ACT.ACT_DTL_SUB_MPRD_ENTP_SELECT");
					put("paramList", new String[] {
							userDto.getVendor_code() // 금형업체
					});
				}
			}));

			model.addAttribute("entpApproveListMap", commonService.callProcedureList(new HashMap<String, Object>() {
				{
					put("procedureName", "PK_RF_ACT.ACT_APPROVE_SUB_MPRD_SELECT");
					put("paramList", new String[] {
							userDto.getVendor_code() // 승인 필요한 대여업체
					});
				}
			}));

		return "pages/web/stock/actListSub";
	}

}
