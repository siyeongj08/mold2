package com.coway.mold.web.login;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.coway.mold.common.BaseController;
import com.coway.mold.common.EtcUtils;
import com.coway.mold.common.MenuSiteUtils;
import com.coway.mold.common.ResultVo;
import com.coway.mold.dto.UserDto;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@SessionAttributes("UserSession")
public class LoginController extends BaseController { 
	
	@RequestMapping(value = { "/", "/login" })
	public String login(HttpServletRequest request, HttpServletResponse response, HttpSession session)
			throws Exception { 

		return "pages/web/login";
	}

	@RequestMapping(value = "/login/ajaxLogin", method = { RequestMethod.POST })
	@ResponseBody
	public ResultVo ajaxLogin(@RequestParam(value="procedureName") String procedureName,
										@RequestParam(value="paramList[]") String[] paramList, Model model) throws Exception {

		ResultVo resultVo = new ResultVo(0);

		Map<String, Object> obj = commonService.callProcedureOne(new HashMap<String, Object>() {
			{
				put("procedureName", procedureName);
				put("paramList", paramList);
			}
		});
		if (obj == null) {
			resultVo.setMessage("ID 또는 PW가 맞지 않습니다");
			log.debug("ID 또는 PW가 맞지 않습니다 ");
			return resultVo;
		}
		UserDto userDto = (UserDto) EtcUtils.convertMapToObject(obj, new UserDto());

		if (userDto == null) {
			// apiLog(params.get("id"), "F", "", request.getRemoteAddr());
			return resultVo;
		}

		/////// 추후 꼭 수정 해야 됩니다.	//////////////////////////////////////////
		// if(userDto.getAuth_group_code() == null){
		// 	userDto.setAuth_group_code("SAVE");
		// }
		/////// 추후 꼭 수정 해야 됩니다.	//////////////////////////////////////////
		 
		// apiLog(params.get("id"), "S", "", request.getRemoteAddr());
		// params.put("status", "Y");

		List<Map<String, Object>> menuListMap = commonService.callProcedureList(new HashMap<String, Object>() {
			{
				put("procedureName", "PK_RF_USER.SITE_MENU_SELECT");
				put("paramList", new String[] {	
						userDto.getAuth_group_code(),
						userDto.getVendor_code()
				});
			}
		});
		userDto.setMenuElement(MenuSiteUtils.MakeMenuElement(menuListMap));

		Map<String, Object> actTrmMap = commonService.callProcedureOne(new HashMap<String, Object>() {
			{
				put("procedureName", "PK_RF_ACT.ACT_ACTIVE_SELECT");
				put("paramList", new String[] {
						userDto.getVendor_code()
				});
			}
		});
		if (actTrmMap != null){
			userDto.setAct_seq(actTrmMap.get("SEQ").toString()); 
			userDto.setAct_yr(actTrmMap.get("ACT_YR").toString());
			userDto.setAct_dgre(actTrmMap.get("ACT_DGRE").toString());


			Map<String, Object> entpSign = commonService.callProcedureOne(new HashMap<String, Object>() {
				{
					put("procedureName", "PK_RF_USER.USER_FILE_SELECT");
					put("paramList", new String[] {
							userDto.getVendor_code()
					});
				}
			});

			userDto.setOfse_apdx_file_no(entpSign.get("OFSE_APDX_FILE_NO").toString());
			userDto.setSign_apdx_file_no(entpSign.get("SIGN_APDX_FILE_NO").toString());
		}
		EtcUtils.setSession(request.getSession(), userDto);
		log.info("New Connected : " + userDto.getVendor_code());

		resultVo.setRetcd(1);
		return resultVo;
	}

	// 20210803 -> 2021-08-03 등으로 날짜 포맷을 변경
	private String insertFormatDate(String date, String separate){
		StringBuilder separatedDate = new StringBuilder(date);
		separatedDate.insert(6, separate).insert(4, separate);
		return separatedDate.toString();
	}

	// 세션이 살아있는지 체크
	@RequestMapping(value = "/login/checkSession", method = { RequestMethod.POST })
	@ResponseBody
	public ResultVo checkSession(HttpServletRequest request) throws Exception {
		ResultVo resultVo = EtcUtils.checkAjaxSession(request);
		if (resultVo != null) {
			return resultVo; // 세션데이터가 있을때
		}
		return new ResultVo(0); // 세션데이터가 없으면

		// ResultVo resultVo = new ResultVo(0);
		// HttpSession session = request.getSession();
		// UserDto userInfoDto = EtcUtils.getSession(session);
		// if ((userInfoDto == null) || (userInfoDto.getId().equals(""))) { // 세션데이터가
		// 없으면
		// resultVo.setRetcd(1);
		// resultVo.setMessage("세션 데이터 없음");
		// return resultVo;
		// }else{
		// resultVo.setRetcd(0);
		// resultVo.setMessage("세션 데이터 있음");
		// return resultVo;
		// }
	}
}
