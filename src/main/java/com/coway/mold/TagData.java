package com.coway.mold;

public class TagData {
    
    String timestamp  ; 
    String txAntennaPort  ; 
    String data   ;  
    int  read   ;
    
    public TagData(){}
 
    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTxAntennaPort() {
        return txAntennaPort;
    }

    public void setTxAntennaPort(String txAntennaPort) {
        this.txAntennaPort = txAntennaPort;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getRead() {
        return read;
    }

    public void setRead(int read) {
        this.read = read;
    }

    
}
