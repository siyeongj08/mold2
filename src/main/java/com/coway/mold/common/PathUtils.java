package com.coway.mold.common;

import java.io.File;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
public class PathUtils {

    String staticPath;

    String UPLOAD_IMG_URL;

    @Value("${UPLOAD_IMG_URL}")
    public void setUPLOAD_IMG_URL(String value) {
        UPLOAD_IMG_URL = value;
        UPLOAD_IMG_PATH  = staticPath + value;

        makeDir( staticPath, value.split("/") ) ; 

    }

    String UPLOAD_IMG_PATH;

    public PathUtils() throws Exception {
    	
        Resource resource = new ClassPathResource("/static");
        staticPath = resource.getURL().getPath();

        if (System.getProperty("os.name").charAt(0) == 'W') {
            staticPath = staticPath.substring(1);
        }
    }

    public String getUpLoadImgUrl() {
        return UPLOAD_IMG_URL;
    }

    public String getUpLoadImgPath() {
        return UPLOAD_IMG_PATH;
    } 

    

    private void makeDir(String uploadPath, String[] paths) { 
		try {
			String os = System.getProperty("os.name").toLowerCase();

			for (String path : paths) {
                if(!path.trim().equals("")){  
                    // logger.info(" uploadPath + path - {}", uploadPath + path);
                    File dirPath = new File(uploadPath + "/" + path);
                    if (!dirPath.exists()) {
                        // 년 월일에 대한 해당 폴더가 존재하지 않으면 폴더 생성
                        if (!os.contains("win")) {
                            Runtime.getRuntime().exec("chmod 777 " + uploadPath + path);
                            dirPath.setExecutable(true, false);
                            dirPath.setReadable(true, false);
                            dirPath.setWritable(true, false);
                        }

                        dirPath.mkdir();
                    }
                    uploadPath = uploadPath + "/" + path ; 
                }
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

    public String getStaticPath() {
        return staticPath;
    }

    public void setStaticPath(String staticPath) {
        this.staticPath = staticPath;
    }

}