package com.coway.mold.common;

import java.awt.image.BufferedImage;
import java.io.File;

import java.awt.Rectangle ; 
import javax.imageio.ImageIO; 


public class ImageUtils {
   
    
	public BufferedImage getObjectImage(String imgPath , int threshold, int basecolor) throws Exception {
            
        File imgf = new File(imgPath);

        BufferedImage img = ImageIO.read(imgf); 

        return img ; 
        //Rectangle rect = getObjectBound(img, basecolor) ;  

        //return getObjectImage(img, threshold, basecolor) ; // img.getSubimage(rect.x -threshold, rect.y-threshold, rect.width+threshold, rect.height+threshold);  
    }

    
	public BufferedImage getObjectImage(BufferedImage img , int threshold, int basecolor) throws Exception { 

        Rectangle rect = getObjectBound(img, basecolor) ;  

        int x0 = 0 ; 
        int y0 = 0 ;  
        int x2 = 0 ;  
        int y2 = 0 ; 

        if ( rect.x -threshold < 0 )   x0 = rect.x ;  
        if ( rect.y-threshold < 0 )    y0 = rect.y ;  
        if ( rect.width+(threshold*2) > img.getWidth() )      x2 = img.getWidth() ;  
        if ( rect.height+(threshold*2) > img.getHeight() )    y2 = img.getHeight() ;  

        return img.getSubimage(x0, y0, x2, y2  );  
    }


    
	public Rectangle getObjectBound(BufferedImage img, int basecolor) throws Exception {
        
        int width = img.getWidth();
        int height = img.getHeight();
        int pixel = 0 ; 
        int red = 0 ; 
        int green = 0 ; 
        int blue = 0 ;  
        int minx =  width ; 
        int miny =  height ; 
        int maxx =  0 ; 
        int maxy =  0 ;  

        for(int w=0 ; w<width ; w++){
            for(int h=0 ; h<height ; h++){
                pixel = img.getRGB(w,h) ;  
                red = (pixel >> 16) & 0xff;
                green = (pixel >> 8) & 0xff;
                blue = (pixel) & 0xff;

                if(red < basecolor  ||  green < basecolor || blue < basecolor ){ 

                    if(minx > w ){
                        minx = w ; 
                    }
                    
                    if(maxx < w ){
                        maxx = w ; 
                    }
                    if(miny > h ){
                        miny = h ; 
                    }
                    
                    if(maxy < h ){
                        maxy = h ; 
                    }  
                } 
            }
        }   
        
        return new Rectangle(minx , miny, maxx-minx, maxy-miny) ; 
    }
    
}
