package com.coway.mold.common;

import java.util.List;
import java.util.Map;

public class MenuSiteUtils {

    public static String MakeMenuElement(List<Map<String, Object>> menuListMap) {
        StringBuilder menuElement = new StringBuilder();
        int mGroup = 0;
        boolean isUlOpen = false;
        for (Map<String, Object> menuMap : menuListMap) {
            if (mGroup != Integer.parseInt(String.valueOf(menuMap.get("MGROUP")))) { // 그룹이 달라지면 
                if (mGroup != 0) { // 최초가 아니면
                    if (isUlOpen == true) {
                        menuElement.append("</ul>");
                        isUlOpen = false;
                    }
                    menuElement.append("</li>");
                }
                menuElement.append("<li class='dropdown'>");
                mGroup = Integer.parseInt(String.valueOf(menuMap.get("MGROUP"))) ;
            }
             
            if (menuMap.get("DEPTH").toString().equals("1")) {

                if (menuMap.get("URL") == null){
                    menuElement.append("<a href='javascript:void(0)' class='nav-link has-dropdown'><i class='" 
                    + menuMap.get("ICON") + "'></i><span class=" + menuMap.get("HTML_CLASS") + ">" + menuMap.get("TITLE") + "</span></a>");
                }else{
                    menuElement.append("<a href='" + menuMap.get("URL") + "' class='nav-link'><i class='" 
                    + menuMap.get("ICON") + "'></i><span class=" + menuMap.get("HTML_CLASS") + ">" + menuMap.get("TITLE") + "</span></a>");
                }
            } else {
                if (isUlOpen == false) {
                    menuElement.append("<ul class='dropdown-menu'>");
                    isUlOpen = true;
                }
                menuElement.append( "<li><a href='" + menuMap.get("URL") + "' class='nav-link'><span class=" + menuMap.get("HTML_CLASS") + ">" + menuMap.get("TITLE") + "</span><i class='" 
                + menuMap.get("ICON") + "'></i></a></li>");
            }
            // <i class='" 
            // + menuMap.get("ICON") + "'></i>

            // menuElement.append( "<li><a href='" + menuMap.get("URL") + "' class='nav-link "+menuMap.get("HTML_CLASS")+"'>" + 
            // menuMap.get("TITLE") + "</a></li>");
        }
        menuElement.append("</li>");

        return menuElement.toString();
    }

}