package com.coway.mold.common;

public class ResultVo {

	private int retcd; // 0=: init 0>: 정상 0<: 에러
	private String message;
	private Object data;

	public ResultVo() {
		retcd = 0;
	}

	public ResultVo(int retcd) {
		this.retcd = retcd;
	}

	public int getRetcd() {
		return retcd;
	}

	public void setRetcd(int retcd) {
		this.retcd = retcd;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
