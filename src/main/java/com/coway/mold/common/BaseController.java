package com.coway.mold.common;

import javax.servlet.http.HttpServletRequest;

import com.coway.mold.db1.CommonService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class BaseController {

	@Autowired
	protected CommonService commonService;

	@Autowired
	protected HttpServletRequest request;

}
