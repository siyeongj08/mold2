package com.coway.mold.common;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.http.Consts;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
//import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.util.NumberUtils;
import org.springframework.util.StringUtils;

import com.coway.mold.dto.ColumnDto;
import com.coway.mold.dto.UserDto;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EtcUtils {

	public static int mapGetInt(Object obj) {
		String str = String.valueOf(obj) ; 
		if(StringUtils.hasLength(str)) { 
			return NumberUtils.parseNumber(str, Integer.class) ; 
		} 

		return 0 ;
	}

	public static String mapGetString(Object obj) {
		String str = String.valueOf(obj) ; 
		if(StringUtils.hasLength(str)) {
			return str ; 
		} 

		return "" ;
	}



	public static String executeJson2(String url, String jsonInString) {
		// int httpConnTimeOut = 20000;
		StringBuffer sb = new StringBuffer();
		CloseableHttpClient client = HttpClients.createDefault();
		BufferedReader reader = null;
		CloseableHttpResponse response = null;
		try {

			HttpPost httpPost = new HttpPost(url);
			httpPost.setHeader("Content-type", "application/json");

			httpPost.setEntity(new StringEntity(jsonInString,
					ContentType.create("application/x-www-form-urlencoded", Consts.UTF_8)));

			response = client.execute(httpPost);

			try {
				reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}
			} catch (Exception e) {
				log.error(e.getMessage());
				e.printStackTrace();
			} finally {
				try {

				} catch (Exception ex) {
					log.error(ex.getMessage());
				}
			}
			return sb.toString();
			
		} catch (Exception ex) {
			log.error(ex.getMessage());
		} finally {
			try {
				if (response != null)
					response.close();
				if (reader != null)
					reader.close();
				if (client != null)
					client.close();
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}
		}
		return null;
	}

	public static void setSession(HttpSession session, UserDto userDto) {
		session.setAttribute("UserDto", userDto);
	}

	public static UserDto getSession(HttpSession session) {
		return (UserDto) session.getAttribute("UserDto");
	}

	public static ResultVo checkAjaxSession(HttpServletRequest request) {
		if (EtcUtils.hasLength(request.getAttribute("ajaxPreHandle"))) {
			return null;
		}

		//log.debug(request.getAttribute("ajaxPreHandle"));
		// log.debug( String.valueOf( request.getAttribute("ajaxPreHandle")) );
		// String ret = StringUtils. ..trimToNull(str).trim
		// .trimAllWhitespace(String.valueOf( request.getAttribute("ajaxPreHandle")));
		// if(ret == null ){
		// return null ;
		// }
		ResultVo resultVo = new ResultVo(0);
		//resultVo.setRetcd(NumberUtils.parseNumber(request.getAttribute("ajaxPreHandle").toString(), Integer.class));// -1

		return resultVo;
	}

	public static void setStartEndDate(Map<String, Object> params, int start, int end) {

		if (EtcUtils.hasLength(params.get("startdt"))) {
			params.put("startdt", LocalDate.now().plusDays(start).format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
		}
		if (EtcUtils.hasLength(params.get("enddt"))) {
			params.put("enddt", LocalDate.now().plusDays(end).format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
		}
	}

	public static String getRootDir() {
		return System.getProperty("user.dir");
	}

	// model.addAttribute("collist", coldto.getColumnlist() );
	// model.addAttribute("colname", coldto.getColumnname().split(",") );

	public static void setColumnModel(String nowUrl, List<ColumnDto> list, Model model) {
		List<ColumnDto> tableInfoList = new ArrayList();
		for (ColumnDto coldto : list) {
			if (nowUrl.startsWith(coldto.getUrl())) {
				tableInfoList.add(coldto);
			}
		}
		model.addAttribute("tableInfoList", tableInfoList);
	}

	public static String[][] stringSplit2Dim(String data, String split1, String split2) {

		String rows[] = data.split(split1);
		String[][] matrix = new String[rows.length][];

		int r = 0;
		for (String row : rows) {
			matrix[r++] = row.split(split2);
		}

		return matrix;
	}

	public static Object convertMapToObject(Map map, Object objClass) {
		String keyAttribute = null;
		String setMethodString = "set";
		String methodString = null;
		Iterator itr = map.keySet().iterator();
		while (itr.hasNext()) {
			keyAttribute = (String) itr.next();
			methodString = setMethodString + keyAttribute.substring(0, 1).toUpperCase()
					+ keyAttribute.substring(1).toLowerCase();
			try {
				Method[] methods = objClass.getClass().getDeclaredMethods();
				for (Method method : methods) {
					if (methodString.equals(method.getName()) && method.getParameterTypes().length == 1) {
						// log.debug("invoke : " + methodString +"+"+
						// method.getParameterTypes()[0].getName());
						if (!method.getParameterTypes()[0].getName().equals("int")) {
							method.invoke(objClass, String.valueOf(map.get(keyAttribute)));
						} else {
							method.invoke(objClass, Integer.parseInt(map.get(keyAttribute).toString()));
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return objClass;
	}

	// String to Json Object
	public static Object JsonToObject(String data) throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(data, Object.class);
	}

	// String to Json Object
	public static String ObjectToJson(Object data) throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(data);
	}

	public static boolean hasLength(Object obj) {
		if (obj == null) {
			return false; 
		} else if (obj.toString().equals("")) {
			return false;
		} else {
			return true;
		}
	}
}
