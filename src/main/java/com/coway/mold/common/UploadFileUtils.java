package com.coway.mold.common;

import java.awt.image.BufferedImage;
import java.io.File;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Iterator;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UploadFileUtils {

	private static Integer WIDTH_SIZE = 100;

	public static Integer getWIDTH_SIZE() {
		return WIDTH_SIZE;
	}

	public static void setWIDTH_SIZE(Integer wIDTH_SIZE) {
		WIDTH_SIZE = wIDTH_SIZE;
	}

	// 1.파일의 저장 경로(uploadPath), 2.원본 파일의 이름(originalName), 3.파일 데이터(byte[])
	public static String uploadFile(String uploadPath, String originalName, byte[] fileData) throws Exception {

		// ★ 1. 고유값 생성
		String savedName = String.valueOf(System.currentTimeMillis()) + "_" + originalName;

		// ★ 2. 년/월/일' 정보 생성
		String savedPath = calcPath(uploadPath);

		// ★ 3. 원본파일 저장
		File target = new File(uploadPath + savedPath, savedName);
		FileCopyUtils.copy(fileData, target);

		// 파일 경로를 -> url 경로로 변경해서 반환
		return makeIcon(uploadPath, savedPath, savedName);
	}

	private static String makeIcon(String uploadPath, String savedPath, String savedName) {
		String iconName = uploadPath + savedPath + File.separator + savedName;
		return iconName.substring(uploadPath.length()).replace(File.separatorChar, '/');
	}

	// 파일이 저장될 '년/월/일' 정보 생성
	private static String calcPath(String uploadPath) {
		Calendar cal = Calendar.getInstance();
		// 역슬래시 + 2017
		String yearPath = File.separator + cal.get(Calendar.YEAR);

		// /2017 +/+ 10 한자리 월 일경우 01, 02 형식으로 포멧
		String monthPath = yearPath + File.separator + new DecimalFormat("00").format(cal.get(Calendar.MONTH) + 1);

		// /2017/10 +/ + 22
		String datePath = monthPath + File.separator + new DecimalFormat("00").format(cal.get(Calendar.DATE));

		// 년월일 폴더 생성하기
		makeDir(uploadPath, yearPath, monthPath, datePath);

		log.info("Date Path makeDir - {} : " + datePath);

		return datePath;
	}

	// 실질 적인 날짜 폴더 생성
	private static void makeDir(String uploadPath, String... paths) {
		if (new File(paths[paths.length - 1]).exists()) {
			// 년 월 일 에서 일 배열 paths 에서 paths -1 은 일 즉 해당일의 폴더가 존재하면 return
			return;
		}

		try {
			String os = System.getProperty("os.name").toLowerCase();

			for (String path : paths) {

				File dirPath = new File(uploadPath + path);
				if (!dirPath.exists()) {
					// 년 월일에 대한 해당 폴더가 존재하지 않으면 폴더 생성
					if (!os.contains("win")) {
						Runtime.getRuntime().exec("chmod 755 " + uploadPath + path);
						dirPath.setExecutable(true, false);
						dirPath.setReadable(true, false);
						dirPath.setWritable(true, false);
					}

					dirPath.mkdir();
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	
	public static ResultFileVo filesave(String fileUploadPath, String orgName, byte[] bytes)
			throws Exception {
		ResultFileVo resultFileVo = new ResultFileVo();
		resultFileVo.setSuccess(false); 

		try {
			String filepath = UploadFileUtils.uploadFile(fileUploadPath, orgName, bytes);
			resultFileVo.setFilePath(filepath); 
			setFileNameExt(resultFileVo , filepath) ;  
			resultFileVo.setOriginalFilename(orgName);
			resultFileVo.setFilesize(bytes.length);
			resultFileVo.setSuccess(true);
		} catch (Exception ex) {
			resultFileVo.setMessage(ex.getMessage());
		} 

		return resultFileVo;
	}

	public static void setFileNameExt(ResultFileVo resultFileVo , String filepath){
		int pos = filepath.lastIndexOf("/") ; 
		if(pos != -1){
			resultFileVo.setFileName( filepath.substring(pos+1) ) ; 
		}

		pos = filepath.lastIndexOf(".") ; 
		if(pos != -1){
			resultFileVo.setFileExt( filepath.substring(pos+1) ) ;  
		}
	} 


	public static ResultFileVo filesave(String fileUploadPath, MultipartHttpServletRequest mulitifilelist)
			throws Exception {
		ResultFileVo resultFileVo = new ResultFileVo();
		resultFileVo.setSuccess(false);

		Iterator<String> iter = mulitifilelist.getFileNames();
		MultipartFile file = null;
		String fieldName = null;
		while (iter.hasNext()) {
			fieldName = (String) iter.next(); // 파일이름, 위에서 file1과 file2로 보냈으니 file1, file2로 나온다.
			if (fieldName.equals("FILE")) {
				file = mulitifilelist.getFile(fieldName); // 저장된 파일 객체
				break;
			}
		}

		if (fieldName == null) {
			log.error("file not found : " + "FILE이 없습니다");
			resultFileVo.setMessage("FILE이 없습니다.");
			return resultFileVo;
		}

		try {
			String filepath = UploadFileUtils.uploadFile(fileUploadPath, file.getOriginalFilename(), file.getBytes());
			resultFileVo.setFilePath(filepath);
			// resultFileVo.setFileUrl( filepath );
			setFileNameExt(resultFileVo , filepath) ; 
			resultFileVo.setOriginalFilename(file.getOriginalFilename());
			resultFileVo.setSuccess(true);
		} catch (Exception ex) {
			resultFileVo.setMessage(ex.getMessage());
		}

		return resultFileVo;
	}

	public static ResultFileVo fileSaveCrop(int threshold, int basecolor, String fileUploadPath,
			MultipartHttpServletRequest mulitifilelist) throws Exception {
		
		ResultFileVo resultFileVo = new ResultFileVo();
		resultFileVo.setSuccess(false);

		Iterator<String> iter = mulitifilelist.getFileNames();
		MultipartFile file = null;
		String fieldName = null;
		
		while (iter.hasNext()) {
			fieldName = (String) iter.next(); // 파일이름, 위에서 file1과 file2로 보냈으니 file1, file2로 나온다.
			if (fieldName.equals("FILE")) {
				file = mulitifilelist.getFile(fieldName); // 저장된 파일 객체
				break;
			}
		}

		
		if (fieldName == null) {
			resultFileVo.setMessage("FILE이 없습니다.");
			log.error("file not found : " + "FILE이 없습니다");
			return resultFileVo;
		}

		try {
			
			//ClassPathResource resource = new ClassPathResource(fileUploadPath + file.getOriginalFilename());
			
			//BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(file.getBytes()));
			BufferedImage bufferedImage = ImageIO.read(file.getInputStream());
			File outputfile = new File(fileUploadPath, System.currentTimeMillis() + file.getOriginalFilename());
			ImageIO.write(bufferedImage, "png", outputfile);
			
			String filepath = UploadFileUtils.uploadFile(fileUploadPath, file.getOriginalFilename(),
					FileUtils.readFileToByteArray(outputfile));

			resultFileVo.setFilePath(filepath);
			setFileNameExt(resultFileVo , filepath) ; 
			resultFileVo.setOriginalFilename(file.getOriginalFilename());
			resultFileVo.setFilesize(file.getBytes().length);
			resultFileVo.setSuccess(true);

		} catch (Exception ex) {
			log.error("file not found : " + ex.getMessage());
			resultFileVo.setMessage(ex.getMessage());
		}

		return resultFileVo;
	}

}