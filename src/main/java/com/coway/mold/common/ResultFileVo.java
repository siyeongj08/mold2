package com.coway.mold.common;
 
import java.io.Serializable;
 
public class ResultFileVo  implements Serializable {
	private static final long serialVersionUID = 4873035241786182838L;

	private boolean success;
	private String message;
	private String originalFilename;
	private String fileUrl;  
	private String filePath;  
	private String fileName;  
	private String fileExt;  
	private long filesize;  

	public ResultFileVo() {}
	
	public ResultFileVo(boolean success, String message, String originalFilename, String fileUrl ) {
		super();
		this.success = success;
		this.message = message;
		this.originalFilename = originalFilename;
		this.fileUrl = fileUrl; 
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getOriginalFilename() {
		return originalFilename;
	}
	public void setOriginalFilename(String originalFilename) {
		this.originalFilename = originalFilename;
	}
	public String getFileUrl() {
		return fileUrl;
	}
	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public long getFilesize() {
		return filesize;
	}

	public void setFilesize(long filesize) {
		this.filesize = filesize;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileExt() {
		return fileExt;
	}

	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	}
	
	 
}
