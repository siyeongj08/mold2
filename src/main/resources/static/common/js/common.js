function ajaxSP(_url, _data, successFunc) {
  $.ajax({
    type: "POST",
    url: _url,
    cachwe: false,
    data: _data,
    success: function (ret) {
      //console.log('success: function');   
      // console.log(ret); 
      if (ret.retcd == 1) {
        successFunc(ret.data);
      } else {
        sessionCheck(ret);
      }
    },
    error: function (request, status, errorThrown) {
      alert("시스템 오류입니다. 잠시 후 다시 시도해 주세요.");
    },
  });
}

function returnDateFormat(date) {
  return (
    date.getFullYear() +
    "-" +
    pad(date.getMonth() + 1, 2) +
    "-" +
    pad(date.getDate(), 2)
  );
  //   var month = (1 + date.getMonth());
  //   month = month >= 10 ? month : '0' + month;
  //   var day = date.getDate();
  //   day = day >= 10 ? day : '0' + day;

  //   return date.getFullYear() + '-' + month + '-' + day;
}

function insertDateFormat(date) {
  if (date.length == 8) {
    return date.slice(0, 4) + "-" + date.slice(4, 6) + "-" + date.slice(6, 8)
  }
  return '';
}

function returnDateAddDays(date, days) {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}

function returnDateAddMonths(date, months) {
  var result = new Date(date);
  result.setMonth(result.getMonth() + months);
  return result;
}

function pad(n, width) {
  n = n + "";
  return n.length >= width ? n : new Array(width - n.length + 1).join("0") + n;
}

Date.prototype.yyyymmdd = function () {
  var mm = this.getMonth() + 1;
  var dd = this.getDate();

  return [
    this.getFullYear(),
    (mm > 9 ? "" : "0") + mm,
    (dd > 9 ? "" : "0") + dd,
  ].join("");
};

Date.prototype.hhmmss = function () {
  var hh = this.getHours();
  var mm = this.getMinutes();
  var ss = this.getSeconds();

  return [
    (hh > 9 ? "" : "0") + hh,
    (mm > 9 ? "" : "0") + mm,
    (ss > 9 ? "" : "0") + ss,
  ].join("");
};

Date.prototype.yyyymmddhhmmss = function () {
  return this.yyyymmdd() + this.hhmmss();
};

Date.prototype.oracleyyyymmdd = function () {
  var mm = this.getMonth() + 1;
  var dd = this.getDate();

  return [
    this.getFullYear(),
    "-",
    (mm > 9 ? "" : "0") + mm,
    "-",
    (dd > 9 ? "" : "0") + dd,
  ].join("");
};

Date.prototype.oraclehhmmss = function () {
  var hh = this.getHours();
  var mm = this.getMinutes();
  var ss = this.getSeconds();

  return [
    (hh > 9 ? "" : "0") + hh,
    ":",
    (mm > 9 ? "" : "0") + mm,
    ":",
    (ss > 9 ? "" : "0") + ss,
  ].join("");
};

Date.prototype.oracleDate = function () {
  return this.oracleyyyymmdd() + " " + this.oraclehhmmss();
};



var defalutNull = "(?)";
var isEmpty = function (...args) {

  if (args[0] == "" || args[0] == null || args[0] == undefined || (args[0] != null && typeof args[0] == "object" && !Object.keys(args[0]).length)) {
    if (args[1] == undefined) {
      return defalutNull;
    }
    return args[1];
  }
  else {
    return args[0];
  }
};


function callProcedureList(procedureName, paramList, successFunc) {
  callProcedure('callProcedureList', procedureName, paramList, successFunc);
}

function callProcedureOne(procedureName, paramList, successFunc) {
  callProcedure('callProcedureOne', procedureName, paramList, successFunc);
}

function callProcedureRet(procedureName, paramList, successFunc) {
  callProcedure('callProcedureRet', procedureName, paramList, successFunc);
}
let db1Url = "/db1/";
function callProcedure(returnType, procedureName, paramList, successFunc) {
  console.log(returnType + " : " + procedureName);
  $.ajax({
    type: "POST",
    url: db1Url + returnType,
    //cache: false,
    dataType: 'json',
    data: {
      procedureName: procedureName,
      paramList: paramList
    },
    contentType: "application/x-www-form-urlencoded; charset=utf-8",
    success: function (ret) {
      if (ret.retcd == 1) {
        successFunc(ret.data);
      } else {
        sessionCheck(ret);
      }
    },
    error: function (request, status, errorThrown) {
      alert("시스템 오류입니다. 잠시 후 다시 시도해 주세요.");
    },
  });
}


