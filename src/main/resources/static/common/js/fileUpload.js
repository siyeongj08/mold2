const _sleep = (delay) => new Promise((resolve) => setTimeout(resolve, delay));

function addFiles(event , imgvar, simgvar , type ){ 
    
    var files = event.target.files;
    //console.log(files[0]);
    var formData = new FormData();
    formData.append("TYPE", type); 
    formData.append("FILE", files[0]); 
    if(!checkImageType(files[0].name)){  
        return ; 
    }

    $.ajax({
        type: "post",
        url: "/web/user/ajaxUploadPic",
        data: formData,
        dataType: "json",
        processData: false,
        contentType: false,
        // beforeSend: function (xhr){
        //     xhr.setRequestHeader("Access-Control-Allow-Origin","*");
        //     xhr.setRequestHeader("Access-Control-Allow-Methods","GET,POST,PUT,DELETE,OPTIONS");
        //     xhr.setRequestHeader("Access-Control-Max-Age","3600");
        //     xhr.setRequestHeader("Access-Control-Allow-Headers","Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization");
        // },
        // 업로드 성공하면
        success: function(data) { 
            console.log(data); 
            
            $("#" + simgvar).val(data.getFileName)  ;  
             
            const timer = async () => {
                await _sleep(1000);
                $("#" + imgvar).attr('src', data.getFileName ); 
            };
            timer();

            //console.log( data.fileUrl ) ;        
        	//var str = "";
            // 이미지 파일이면 썸네일 이미지 출력 
        	// if($("#" + imgvar).val() != "" ){
        	// 	$("#" + divname).html('') ; 
	        // }
        	// //$("#oneFile").remove();
            // if(checkImageType(data.originalFilename)){              	
            //     str = "<div id='oneFile'><a href='/ajax/displayFile.do?fileName="+getImageLink(data.fileUrl)+"' target='_blank'>"; //원본 이미지 새창에서 보여주기
            //     str += "<img src='/ajax/displayFile.do?fileName="+data.fileUrl+"'></a>"; //썸네일 이미지
	        //     //str += "<span data-src="+data.fileUrl+" onclick='deleteFiles(this)' style='cursor:pointer'>[삭제]</span>"; //삭제 버튼
	        //     //str += "<input type='hidden' name='_imgUrl' value='"+ getImageLink(data.fileUrl) +"'>"; //원본 경로
	        //    // str += "<input type='hidden' name='_simgUrl' value='"+ data.fileUrl +"'>
            //     str += "</div>"; //썸네일 경로  
	        //     $("#" + divname).append(str); 
	            
	        //     $("#" + imgvar).val( getImageLink(data.fileUrl) ) ; 
	        //     if(simgvar != ''){
	        //     	$("#" + simgvar).val( data.fileUrl  ) ;
	        //     }
            // }
        },
        error: function (request, status, errorThrown) {
            alert("파일 업로드에 실패하였습니다.");
        }
    });
}

function deleteFiles(event){
	var that = $(event); // 여기서 this는 클릭한 span태그

    $.ajax({
        url: "/ajax/deleteFile.do",
        type: "post",
        // data: "fileName="+$(this).attr("date-src") = {fileName:$(this).attr("data-src")}
        // 태그.attr("속성")
        data: {fileName:$(event).attr("data-src")}, // json방식
        dataType: "text",
        success: function(result){
            if( result == "deleted" ){
                // 클릭한 span태그가 속한 div를 제거
                that.parent("div").remove();
                $('#fileupload').val("");
                
            }
        }
    });
}



function getOriginalName(fileName) {
    // 이미지 파일이면
    if(checkImageType(fileName)) {
        return; // 함수종료
    }
    // uuid를 제외한 원래 파일 이름을 리턴
    var idx = fileName.indexOf("_")+1;
        return fileName.substr(idx);
    }
    
 // 이미지파일 링크 - 클릭하면 원본 이미지를 출력해주기 위해
function getImageLink(fileName) {
    // 이미지파일이 아니면
    if(!checkImageType(fileName)) { 
        return; // 함수 종료 
    }
    // 이미지 파일이면(썸네일이 아닌 원본이미지를 가져오기 위해)
    // 썸네일 이미지 파일명 - 파일경로+파일명 /2017/03/09/s_43fc37cc-021b-4eec-8322-bc5c8162863d_spring001.png
    var front = fileName.substr(0, 12); // 년원일 경로 추출
    var end = fileName.substr(14); // 년원일 경로와 s_를 제거한 원본 파일명을 추출
    console.log(front); // /2017/03/09/
    console.log(end); // 43fc37cc-021b-4eec-8322-bc5c8162863d_spring001.png
    // 원본 파일명 - /2017/03/09/43fc37cc-021b-4eec-8322-bc5c8162863d_spring001.png
    return front+end; // 디렉토리를 포함한 원본파일명을 리턴
    }
 
 // 이미지파일 형식을 체크하기 위해
function checkImageType(fileName) {
    // i : ignore case(대소문자 무관)
    var pattern = /jpg|gif|png|jpeg/i; // 정규표현식
    return fileName.match(pattern); // 규칙이 맞으면 true
}
