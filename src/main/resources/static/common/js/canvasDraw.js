
    function canvasInit(canvasId, prevId, clearId ) {
        var _drawCanvas = document.getElementById(canvasId);
        var drawCanvas = $('#' + canvasId) ; 

		var drawBackup = new Array();
		if (typeof _drawCanvas.getContext == 'function') { 
			var ctx = _drawCanvas.getContext('2d');
			var isDraw = false;
			var width = $('#width').val();
			var color = $('#color').val();
			var pDraw = drawCanvas.offset();
			var currP = null;

			var targetTop = drawCanvas.offset().top;
			var targetLeft = drawCanvas.offset().left;

			$('#width').bind('change', function(){ width = $('#width').val(); });
			$('#color').bind('change', function(){ color = $('#color').val(); });

			//저장된 이미지 호출
			if (localStorage['imgCanvas']) {
				loadImage();
			} else {
				ctx.clearRect(0, 0, drawCanvas.width, drawCanvas.height);
			}

			// Event (마우스)
			drawCanvas.bind('mousedown', function(e) {
				if (e.button===0) {
					saveCanvas();
					e.preventDefault();
					ctx.beginPath();
					isDraw = true;
				}
			});
			drawCanvas.bind('mousemove', function(e) {
				var event = e.originalEvent;
				e.preventDefault();
				currP = { X:event.offsetX, Y:event.offsetY };
				if(isDraw) draw_line(currP);
			});

			drawCanvas.bind('mouseup', function(e) {
				e.preventDefault();
				isDraw = false;
			});

			drawCanvas.bind('mouseleave', function(e) {
				isDraw = false;
			});

			// Event (터치스크린)
			drawCanvas.bind('touchstart', function(e) {
				saveCanvas();
				e.preventDefault();
				ctx.beginPath();
			});

			drawCanvas.bind('touchmove', function(e) {
				var event = e.originalEvent;
				e.preventDefault();


				currP = { X:event.touches[0].pageX - targetLeft, Y:event.touches[0].pageY - targetTop};
				// currP = { X:event.touches[0].pageX - leftOffset, Y:event.touches[0].pageY - topOffset};
				// currP = { X:event.touches[0].screenX, Y:event.touches[0].screenY };
				draw_line(currP);
			});

			drawCanvas.bind('touchend', function(e) {
				e.preventDefault();
			});

			// 선 그리기
			function draw_line(p) {
				ctx.lineWidth = width;
				ctx.lineCap = 'round';
				ctx.lineTo(p.X, p.Y);
				ctx.moveTo(p.X, p.Y);
				ctx.strokeStyle = color;
				ctx.stroke();
			}

			function loadImage() { // reload from localStorage
				var img = new Image();
				img.onload = function() {
					ctx.drawImage(img, 0, 0);
				}
				img.src = localStorage.getItem('imgCanvas');
			}

			// function saveImage() { // save to localStorage
			// 	var canvas = document.getElementById('drawCanvas');
			// 	localStorage.setItem('imgCanvas', canvas.toDataURL('image/png'));
			// 	var img =  document.getElementById('saveImg');
			// 	img.src = canvas.toDataURL('image/png');
			// 	var tmp = $('<a>').attr('download', 'test.png').attr('href', img.src);
			// 	tmp[0].click();
			// 	tmp.remove();
			// }

			function clearCanvas() {
				ctx.clearRect(0, 0, _drawCanvas.width, _drawCanvas.height);
				ctx.beginPath();
				localStorage.removeItem('imgCanvas');
			}

			function saveCanvas() {
				drawBackup.push(ctx.getImageData(0, 0, _drawCanvas.width, _drawCanvas.height));
			}

			function prevCanvas() {
				ctx.putImageData(drawBackup.pop(), 0, 0);
            }
            
			$('#' + prevId).click(function() {
				prevCanvas();
			});

			$('#' + clearId).click(function() {
				clearCanvas();
			});

			// $('#btnSave').click(function() {
			// 	saveImage();
			// });
		}
	}  