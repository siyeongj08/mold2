// function setDataTableColumnName(dataTableId, columnNameList) {
//   columnNameList.forEach(function (column) {
//     // *제이쿼리 DataTable에 동적으로 컬럼헤더(thead)를 추가하려면 꼭 thead 하위의 tr에 th를 추가하여야 함
//     $("#" + dataTableId + " > thead > tr").append("<th> " + column + " </th>");
//   });
// }

const PAGE_MAX_COUNT = 30;

function changeColorRow(dataTable, rowIndex, color) {
  var row = dataTable.row(rowIndex).node();
  //var row = dataTable.row(":eq(" + rowIndex + ")").node();
  $(row).removeClass("bg-primary bg-secondary bg-success bg-info bg-warning bg-danger bg-light bg-dark");
  $(row).addClass(color);
}

function removeRow(dataTable, rowIndex) {
  return dataTable
    .row(":eq(" + rowIndex + ")")
    .remove()
    .draw(false);
}

function dataSpread(dataTable, listMap) {
  // 받아온 데이터
  dataTable.clear();
  for (var obj of listMap) {
    //console.log(obj);
    dataTable.row.add(obj);
  }
  dataTable.draw(false);
}
function getOrderNum(meta) {
  return meta.row + meta.settings._iDisplayStart + 1;
}

var renderRowNum = function (data, type, row, meta) {
  return getOrderNum(meta);
};

function saveStateChangedIndex(index, element) {
  console.log("saveChangedIndex : " + index);
  changedIndexSet.add(index);
  if (element != null) {
    //toast('#' + arr[1] + index);
    $("#" + element + index).css("background", "#fff");
    //tableMain.cell({row : index, column : 3}).data(yellowDot);
  }
}

function getSectionSelect(index) {
  return (
    `<select onchange="addEventStateChange(` +
    index +
    `)" class="form-control-sm" name="sectionSelect` +
    index +
    `" id="sectionSelect` +
    index +
    `">
  <option value="0">미분류</option>
  <option value="1">1</option>
  <option value="2">2</option>
  <option value="3">3</option>
  <option value="4">4</option>
  <option value="5">5</option>
  <option value="6">6</option>
  <option value="7">7</option>
  <option value="8">8</option>
  <option value="9">9</option>
  </select>`
  );
}

function getUsingStateSelect(index) {
  return (
    `<select onchange="saveChangedIndex(` +
    index +
    `)" class="form-control-sm" name="usingState` +
    index +
    `" id="usingState` +
    index +
    `">
  <option value="0" hidden select>?</option>
  <option value="A">사용</option>
  <option value="B">단종</option>
  <option value="C">SPEC변경 미사용</option>
  <option value="D">리피트 중복 미사용</option>
  </select>`
  );
}

function getUsingStateRadio(index) {
  return (
    `
  <div class="btn-group" id="groupUsingState` +
    index +
    `" role="group">
    <input type="radio" value="0" name="radioUsingState` +
    index +
    `" checked style="display: none;"/>
    <label class="wordInRadio"><input type="radio" value='A' class="radioState" onclick='addEventStateChange(` +
    index +
    `, "groupUsingState")' name="radioUsingState` +
    index +
    `"/><span class="spanUsingState1">A</span></label>
    <label class="wordInRadio"><input type="radio" value='B' class="radioState" onclick='addEventStateChange(` +
    index +
    `, "groupUsingState")' name="radioUsingState` +
    index +
    `"/><span class="spanUsingState2">B</span></label>
    <label class="wordInRadio"><input type="radio" value='C' class="radioState" onclick='addEventStateChange(` +
    index +
    `, "groupUsingState")' name="radioUsingState` +
    index +
    `"/><span class="spanUsingState3">C</span></label>
    <label class="wordInRadio"><input type="radio" value='D' class="radioState" onclick='addEventStateChange(` +
    index +
    `, "groupUsingState")' name="radioUsingState` +
    index +
    `"/><span class="spanUsingState4">D</span></label>
  </div>
  `
  );
}

function getToolsStateSelect(index) {
  return (
    `<select onchange="saveChangedIndex(` +
    index +
    `)" class="form-control-sm" name="toolsState` +
    index +
    `" id="toolsState` +
    index +
    `">
  <option value="0" hidden select>?</option>
  <option value="A">상</option>
  <option value="B">중</option>
  <option value="C">하</option>
  <option value="D">노후</option>
  </select>`
  );
}

function getToolsStateRadio(index) {
  return (
    `
  <div class="btn-group" id="groupToolsState` +
    index +
    `" role="group">
    <input type="radio" value="0" name="radioToolsState` +
    index +
    `" checked style="display: none;"/>
    <label class="wordInRadio"><input type="radio" value='A' class="radioState" onclick='addEventStateChange(` +
    index +
    `, "groupToolsState")' name="radioToolsState` +
    index +
    `"/><span class="spanToolsState1">A</span></label>
    <label class="wordInRadio"><input type="radio" value='B' class="radioState" onclick='addEventStateChange(` +
    index +
    `, "groupToolsState")' name="radioToolsState` +
    index +
    `"/><span class="spanToolsState2">B</span></label>
    <label class="wordInRadio"><input type="radio" value='C' class="radioState" onclick='addEventStateChange(` +
    index +
    `, "groupToolsState")' name="radioToolsState` +
    index +
    `"/><span class="spanToolsState3">C</span></label>
    <label class="wordInRadio"><input type="radio" value='D' class="radioState" onclick='addEventStateChange(` +
    index +
    `, "groupToolsState")' name="radioToolsState` +
    index +
    `"/><span class="spanToolsState4">D</span></label>
  </div>
  `
  );
}

var approveBtn = function (data, type, row, meta) {
  var index = getOrderNum(meta);
};

function getRowIndex(dataTable, columnIndex, value) {
  return dataTable.column(columnIndex).data().indexOf(value);
}

function getSelectedRowsIndex(dataTable) {
  return dataTable.rows({ selected: true }).indexes().toArray();
}

function getRowData(dataTable, rowIndex) {
  return dataTable
    .rows(":eq(" + rowIndex + ")")
    .data()
    .toArray()[0];
}

function getSelectedRowsData(dataTable) {
  return dataTable.rows({ selected: true }).data().toArray();
}

function getRowsData(dataTable) {
  return dataTable.rows().data().toArray();
}

var inputText = `<input type="text" name="moldWeight" value='' style="width: 150px;"></input>`;

function addEventSelectAll(dataTable, selectBoxHeaderName) {
  $("input[name=" + selectBoxHeaderName + "]")
    .on("click", function () {
      if ($("th.select-checkbox").hasClass("selected")) {
        dataTable.rows().deselect();
        $("th.select-checkbox").removeClass("selected");
      } else {
        dataTable.rows().select();
        $("th.select-checkbox").addClass("selected");
      }
    })
    .on("select deselect", function () {
      ("Some selection or deselection going on");
      if (
        dataTable
          .rows({
            selected: true,
          })
          .count() !== dataTable.rows().count()
      ) {
        $("th.select-checkbox").removeClass("selected");
      } else {
        $("th.select-checkbox").addClass("selected");
      }
    });
}

var changedIndexSet = new Set(); // 저장되어야할 변경된 실사 Set
function saveChangedIndex(index) {
  console.log("saveChangedIndex : " + index);
  changedIndexSet.add(index);
}


function getNumFilteredRows(dataTable){
  var info = dataTable.page.info();
  return info.recordsDisplay;
}

function setRowCount(id, pre, dataTable){
  $(id).html(pre +  getNumFilteredRows(dataTable));
}

// var outRequestReason = `<select name="reason" class="form-control" style="min-width: 150px">
// <option value="0" hidden select>선택</option>
// <option value="1">수리</option>
// <option value="2">개조</option>
// <option value="3">양산처변경</option>
// <option value="4">시험사출</option>
// <option value="5">수기입력</option>
// </select>`;

// var customReason = `<input type="text" name="customReason" class='form-control' value='' style="min-width: 150px; display=none;"></input>`;
