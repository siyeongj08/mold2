let latestAppVersionPDA = "1.00";
/* 운영변경 */
let latestAppVersionMobile = "1.23";

function checkMobile() {
	var varUA = navigator.userAgent.toLowerCase(); //userAgent 값 얻기
	console.log(varUA);
	if (varUA.indexOf("android") > -1) {
		return 1;
	} else {
		return 3;
	}
}


function setNaviAddress(addr) {

    $('#naviNowAddr').text(addr);
    /*iziToast.warning({
        title: "현주소",
        message: addr,
        position: "bottomCenter",
    });*/
    
}
	
function callApp(func, param) {
	//모바일 앱으로 접속하였는지 체크
	console.log("callApp : " + func + ", " + param);
	
	try {
		switch (checkMobile()) {
			case 1:
				Android.callAndroid(func, param);
				break;
			case 2:
				Flutter.postMessage(func);
				break;
			case 3:
				scanTag(param, 0, 0);
				break;
			default:
				break;
		}
	} catch (err) {
		console.log(err);
	}
}

var audioSuccess = new Audio("/qrScan/blop.mp3");
var prevQrTime = 0; //(new Date()).getTime() ;   // 이전에 QR 읽은 시간 
let MAXQRSECOND = 5 * 1000;

function scanQR(qrtype, qrvalue, lat, lng) {
	showQrVal(qrvalue);
	if (isEmpty(qrvalue, '') == '') {
		return;
	}
	audioSuccess.play();
	insertQRData(qrvalue, lat, lng);
}

function scanTag(tagData, lat, lng) {
	showQrVal(tagData);
	if (isEmpty(tagData, '') == '') {
		return;
	}
	insertQRData(tagData.substring(0, 11), lat, lng);
}

function insertManualData(qrCode) {
	callApp('insertManualData', qrCode)
}


function showQrVal(qr) {
}


function setStartURL() {
	callApp("setStartURL", "login,web/main/dashboard");
}
